
if( document.contactform.names.value == "" ) {
	alert( "Please provide your name!" );
	document.contactform.names.focus() ;
	return false;
 }
 if( document.contactform.emails.value == "" ) {
	alert( "Please provide your Email!" );
	document.contactform.emails.focus() ;
	return false;
 }
 if( document.contactform.messages.value == "" || isNaN( document.contactform.messages.value ) ||
	document.contactform.messages.value.length != 5 ) {
	
	alert( "Please write a message." );
	document.contactform.messages.focus() ;
	return false;
 }
 return( true );


